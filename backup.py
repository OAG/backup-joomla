import zipfile
import os
import time
from subprocess import STDOUT, check_call, CalledProcessError
import boto3
import yaml
import logging.config

FILENAME_DUMP = "db_dump.sql"

logger = logging.getLogger(__name__)


def prefijo(filestamp):
    fecha=filestamp.split("-")
    if fecha[1]=='12' and fecha[2] == '31':
        prefijo = "anual"
    else:
        prefijo = "diaria"
    logger.info(prefijo)
    return prefijo


def backup(user, password, target):
    logger.info("Starting backup DB")
    with open(target + "\\" + FILENAME_DUMP, 'w') as out:
        try:
            check_call(['mysqldump.exe', '--single-transaction',
                        '--user=' + user, '--password=' + password, '--all-databases'],
                       stdout=out, stderr=STDOUT)
            logger.info("Backup created")
        except CalledProcessError as e:
            logger.error(e)


def compress(filename, sources, target):
    logger.info("Compressing backup")
    filename_zip = target + "\\" + filename + ".zip"

    zipf = zipfile.ZipFile(filename_zip, 'w', zipfile.ZIP_DEFLATED)
    zipf.write(target + "\\" + FILENAME_DUMP, "db\\" + FILENAME_DUMP)

    for path in sources:
        for root, dirs, files in os.walk(path):
            for file in files:
                zipf.write(os.path.join(root, file))
    zipf.close()
    logger.info("Backup compressed")


def upload_to_s3(filename, path, bucket_name, aws_access_key_id, aws_secret_access_key):
    logger.info("Uploading backup to S3")
    s3 = boto3.client('s3', aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
    s3.upload_file(path + "\\" + filename + ".zip", bucket_name, filename + ".zip")
    logger.info("Backup uploaded")


def clean(path, delay):
    now = time.time()
    for file in os.listdir(path):
        filename = os.path.join(path, file)
        if os.stat(filename).st_mtime < now - delay:
            os.remove(filename)

    logger.info("Cleaned")


if __name__ == "__main__":
    cwd = os.getcwd()
    config_file = os.path.join(cwd, "config.yml")
    with open(config_file, 'r') as f:
        config = yaml.load(f)

    # Concatena path actual con target y crea el directorio si no existe
    aux = config['backup']['target']
    config['backup']['target'] = os.path.join(cwd, aux)
    if not os.path.isdir(config['backup']['target']):
        os.mkdir(config['backup']['target'])

    # Concatena path actual con log y crea el directorio si no existe
    aux = config['logs']['path']
    config['logs']['path'] = os.path.join(cwd, aux)
    if not os.path.isdir(config['logs']['path']):
        os.mkdir(config['logs']['path'])

    aux = config['logs']['config']
    config['logs']['config'] = os.path.join(cwd, aux)
    with open(config['logs']['config'], 'r') as f:
        conf = yaml.load(f)
        conf['handlers']['file']['filename'] = os.path.join(config['logs']['path'],
                                                            conf['handlers']['file']['filename'])
        logging.config.dictConfig(conf)

    path = config['backup']['target']
    filestamp = time.strftime('%Y-%m-%d_%H-%M-%S')
    filename = prefijo(filestamp) + "-" + filestamp + "-db"
    # filename = filestamp + "-db"

    backup(target=path, **config['db'])
    compress(filename=filename, **config['backup'])
    upload_to_s3(filename=filename, path=path, **config['aws'])
    clean(path=path, delay=config['delay'])
